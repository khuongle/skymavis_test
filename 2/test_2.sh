#!/bin/bash

opts=$@

main (){
	get_opts
	check_opts
	get_vulnerabilities
}

get_opts (){
for opt in $opts;do #loop each option in $opts
	case $opt in
		--token=*)
			token="${opt#*=}" #set Quay token
			;;
		--host=*)
			host="${opt#*=}" #set Quay host
			;;
		--repository=*)
			repository="${opt#*=}" #set Quay repository
			;;
		--organization=*)
			organization="${opt#*=}" #set Quay organization
			;;
		--tag=*)
			tag="${opt#*=}" #set Quay organization
			;;
		*) # exit when invdalid
			echo "Error: Invalid options $opt. Please use vailable options: --host= --token= --repository= --organization= --tag="
			exit 1
	esac
	shift
done
}

check_opts (){
if [ ! -z $token or ! -z $host]; then
	echo "Quay Host or Token is missing. please use options --host and --token"
	exit 1;
fi
if [ -z $repository && -z $organization && -z $tag]; then
	echo "Use repository: $repository andh organization: $organization from input"
	input=true
else
	echo "Use repository and organization in list.json"
	input=false
fi
}

get_vulnerabilities (){
if $input ; then
	manifestref=`curl -X GET https://$host/api/v1/repository/$repository/tag/$tag/images -H 'Authorization: Bearer '"$token"'' | grep ^Manifest | cut -d':' -f2 | tr -d '"'`
	curl -X GET https://$host/api/v1/repository/$repository/manifest/$manifestref/security -H 'Authorization: Bearer '"$token"'' >> vulnerabilities.list
fi
}

main $@
