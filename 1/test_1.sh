#!/bin/bash
#1. Do the test base on requirements
nat_instances=("us-west1-a" "us-west1-b" "us-west1-c")

subnets=("us-west1-a" "us-west1-b" "us-west1-b" "us-west1-c")

for instance in "${nat_instances[@]}"; do
	instance_status=`gcloud compute instances describe $instance | grep status | awk '{print $2}'`
	zone=`gcloud compute instances describe $instances | grep ^zone | cut -d'/' -f9`
	if [ $instance_status == "RUNNING" ]; then
		 gcloud compute routes create route-$zone-$instance --destination-range=0.0.0.0/0 --next-hop-instance=$instance --next-hop-instance-zone=$zone --priority=900 --tags=$zone #it requires all instances in this AZ is already have network tag: $zone
	else
		case $instance in
			us-west1-a)
				for next_instance in "${nat_instances[@]}"; do
					if [ $next_instance != "$instance" ]; then
						next_status=`gcloud compute instances describe $next_instance | grep ^status | awk '{print $2}'`
						next_zone=`gcloud compute instances describe $next_instances | grep ^zone | cut -d'/' -f9`
						if [ $next_status == "RUNNING" ]; then
							gcloud compute routes create route-$zone-$next_instance --destination-range=0.0.0.0/0 --next-hop-instance=$next_instance --next-hop-instance-zone=$next_zone --priority=900 --tags=$zone #it requires all instances in this AZ us-west1-a is already have network tag: $zone=us-west1-a
						fi
					fi
					break
				done
			us-west1-b)
				for next_instance in "${nat_instances[@]}"; do
					if [ $next_instance != "$instance" ]; then
						next_status=`gcloud compute instances describe $next_instance | grep ^status | awk '{print $2}'`
						next_zone=`gcloud compute instances describe $next_instances | grep ^zone | cut -d'/' -f9`
						if [ $next_status == "RUNNING" ]; then
							gcloud compute routes create route-$zone-$next_instance --destination-range=0.0.0.0/0 --next-hop-instance=$next_instance --next-hop-instance-zone=$next_zone --priority=900 --tags=$zone #it requires all instances in this AZ us-west1-b is already have network tag:$zone=us-west1-b
						fi
					fi
					break
				done
			us-west1-c)
				for next_instance in "${nat_instances[@]}"; do
					if [ $next_instance != "$instance" ]; then
						next_status=`gcloud compute instances describe $next_instance | grep ^status | awk '{print $2}'`
						next_zone=`gcloud compute instances describe $next_instances | grep ^zone | cut -d'/' -f9`
						if [ $next_status == "RUNNING" ]; then
							gcloud compute routes create route-$zone-$next_instance --destination-range=0.0.0.0/0 --next-hop-instance=$next_instance --next-hop-instance-zone=$next_zone --priority=900 --tags=$zone #it requires all instances in this AZ us-west1-c is already have network tag: $zone=us-west1-c
						fi
					fi
					break
				done
		esac
	fi
done

#########
#Bonus:
#if all subnets allocated to each NAT instance have the same total weight (and same priority), it will balance all egress traffic of all subnets to all NAT instances


#########
#2. My Solution:
#The default static route in each subnet should be stable, cause it cannot be modified after created.
#In this case, I will create all the default static route to all nat instances with same priority.
#when routes have the same priority and same network tags, it will find the nearest nat instances in the same AZ first if it is available and will switch to another available nat instance if the nearest is not available.
#So I will only creates the 3 routes only and do nothing more:

###script 2###
#!/bin/bash
#for instance in "${nat_instances[@]}"; do
#	zone=`gcloud compute instances describe $instances | grep ^zone | cut -d'/' -f9`
#	gcloud compute routes create route-$instance --destination-range=0.0.0.0/0 --next-hop-instance=$instance --next-hop-instance-zone=$zone --priority=900 --tags=private
#done
